class Scoreboard:
	def __init__(self, liga, temporada, jornada, partidos):
		self.liga = liga
		self.temporada = temporada
		self.jornada = jornada
		self.partidos = partidos
	
	# Redefinicion del metodo str
	def __str__(self):
		#crear listas de partidos temporales
		partidosFin=[]
		partidosSinComenzar=[]
		partidosEnCurso=[]
		
		partidosFinStr=""
		partidosSinComenzarStr=""
		partidosEnCursoStr=""
		
		#Imprinir encabezado
		encabezado=f"#{self.liga} {self.temporada} Scores. {self.jornada}. \n"
		tags=f"#fifaworldcup #worldcup #fifaworldcup2022 #football #soccer #futbol #mundialqatar #mundialqatar2022 #qatar2022 #mastodonfc \n" 
		
		# generar listas en funcion del eatado
		for ps in self.partidos:
		    if ps.finalizado:
		        partidosFin.append(ps)
		    elif ps.estado == "STATUS_SCHEDULED":
		    #elif ps.estado != "STATUS_SCHEDULED":
		        partidosSinComenzar.append(ps)  
		    else:
			    partidosEnCurso.append(ps)
		
		# Imprimir listas si tienen algun elemento
		
		if len(partidosFin) > 0:
		    partidosFinStr=f"Ended Games:\n"
		    for pf in partidosFin:
		        partidosFinStr=f"{partidosFinStr}{pf}\n"
		        
		if len(partidosEnCurso) > 0:
		    partidosEnCursoStr=f"Running Games:\n"
		    for pec in partidosEnCurso:
		        partidosEnCursoStr=f"{partidosEnCursoStr}{pec}\n"
		        
		if len(partidosSinComenzar) > 0:
		    partidosSinComenzarStr=f"Upcoming Games:\n"
		    for psc in partidosSinComenzar:
		        partidosSinComenzarStr=f"{partidosSinComenzarStr}{psc}\n"	       
		
		#juntar todo para tener la salida
		salida=f"{encabezado}\n{partidosFinStr}{partidosEnCursoStr}{partidosSinComenzarStr} \n{tags}"
		
		# Se usaba para imprimir todos los partidos
		#for p in self.partidos:
			#salida=f"{salida} {p}\n"	
			
		return salida
	
	# Metodo para dividir en trozos el texto de un scoreboard
	def divideScoreboard(self,tam):
		#print("Funcion Divide")
		scorebrdStrList=self.__str__().splitlines(True)
		sumatam=0
		scorebrdStrSalida = []
		strTemp=""
		#print(len(strTemp))
		for s in scorebrdStrList:
		    #print("tratando "+s)
		    if ((sumatam + len(s)) < tam):
		        strTemp += s
		        sumatam = len(strTemp)
		        if s == scorebrdStrList[-1]:
		            scorebrdStrSalida.append(strTemp)		            
		    else:
		        scorebrdStrSalida.append(strTemp)
		        strTemp=f"{s}"
		        sumatam=len(strTemp)
		        if s == scorebrdStrList[-1]:
		            scorebrdStrSalida.append(strTemp)
		#Incluir la ultima lista
		
		return scorebrdStrSalida
		        
