from relojjuego import *
from equipo import *
from partido import *
from scoreboard import *
import dateutil.parser

# Esta clase traduce el json dd Espn en objetos mediante el metodo generaScoreboard. Si se usara una API diferente necesitaria su propia clase
class RecolectorESPN:
	def __init__(self,datos):
		self.datos = datos
		
	def imprimeInfo(self):
		info=self.datos
		liga=info['leagues'][0]
		print(liga['name'])
		temporada=info['season']
		print(temporada['year'])
		#jornada=info['week']
		jornada=info['season']['type']['name']
		print(jornada)
		partidos=info['events']
		for p in partidos:
			print(p['shortName'])
	
	def generaScoreboard(self):
		info=self.datos
		liga=info['leagues'][0]['name']
		temporada=info['season']['year']
		#jornada=info['week']['number']
		jornada=info['leagues'][0]['season']['type']['name']
		
		#generar partidos
		listaPartidos = []
		for p in info['events']:
			idPartido=p['id']
			nombrePartido=p['shortName']
			#Realmente fecha inicio es el campo shortDetail. Partido sin comenzar muestra fecha inicio. Partido en curso muestra reloj de juego
			#fechaInicioPartido=p['status']['type']['shortDetail']
			#fechaInicioPartido=p['date']
			dateTimeFechaInicioPartido=dateutil.parser.isoparse(p['date'])
			fechaInicioPartido=dateTimeFechaInicioPartido.strftime("%d/%m/%Y %H:%M") + " UTC"
			finalizadoPartido=p['status']['type']['completed']
			#finalizadoPartido=True
			estadoPartido=p['status']['type']['name']
			progresoPartido=p['status']['type']['shortDetail']
			relojPartido=RelojJuego(p['status']['displayClock'])
			
			equiposApi=p['competitions'][0]['competitors']
			#print(type(equiposApi))
			
			#Estos equipos hay q convertilos a objeto equipo
			equiposPartido = []
			for e in equiposApi:
				idEquipo=e['id']
				nombreCortoEquipo=e['team']['abbreviation']
				nombreCompletoEquipo=e['team']['displayName']
				puntuacionEquipo=e['score']
				if p['status']['type']['name'] == "STATUS_FINAL_PEN":
					puntuacionPenaltisEquipo=e['shootoutScore']
				else:
					puntuacionPenaltisEquipo=0
				if e['homeAway'] == "home":
					equipoCasa=True
				else:
					equipoCasa=False
				# Generar la lista de los equipos
				equiposPartido.append(Equipo(idEquipo,nombreCortoEquipo,nombreCompletoEquipo,equipoCasa,puntuacionEquipo,puntuacionPenaltisEquipo))
			
			#Con todo listo generar la lista de partidos
			listaPartidos.append(Partido(idPartido,nombrePartido,fechaInicioPartido,estadoPartido,equiposPartido,relojPartido,finalizadoPartido,progresoPartido))	
			scoreboardESPN = Scoreboard(liga,temporada,jornada,listaPartidos)
			
		return scoreboardESPN
